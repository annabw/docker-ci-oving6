import * as React from 'react';
import { TaskList, TaskNew, TaskDetails, TaskEdit } from '../src/task-components';
import { shallow } from 'enzyme';
import { Form, Button, Column } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/task-service', () => {
  class TaskService {
    getAll() {
      return Promise.resolve([
        { id: 1, title: 'Les leksjon', description: 'Ta notater', done: true },
        {
          id: 2,
          title: 'Møt opp på forelesning',
          description: 'Webutvikling. Husk laptop og lader',
          done: false,
        },
        { id: 3, title: 'Gjør øving', description: 'Frist: Fredag 19. november', done: false },
      ]);
    }

    get() {
      return Promise.resolve({
        id: 1,
        title: 'Les leksjon',
        description: 'Ta notater',
        done: true,
      });
    }

    update() {
      return Promise.resolve();
    }

    delete() {
      return Promise.resolve();
    }

    create() {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }
  }
  return new TaskService();
});

// Testing av komponenter:
describe('Task component tests', () => {
  // Test nr. 1
  test('TaskList draws correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/tasks/1">Les leksjon</NavLink>,
          <NavLink to="/tasks/2">Møt opp på forelesning</NavLink>,
          <NavLink to="/tasks/3">Gjør øving</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  // Del 2: Test med wrapper
  // Test nr. 2
  test('TaskDetails tegner riktig', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Venter på at resten av koden har kjørt
    setTimeout(() => {
      // Forventer at inneholder følgende elementer:
      expect(
        wrapper.containsAllMatchingElements([
          <Column>Les leksjon</Column>,
          <Column>Ta notater</Column>,
          // @ts-ignore - ignorerer form.checkbox
          <Form.Checkbox checked={true} />,
        ])
      ).toEqual(true);
      done(); // Indikerer at testen er ferdig
    });
  });

  // Del 2: Test med snapshots
  // Test nr. 3
  test('TaskDetails tegner riktig med snapshots', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();
      done();
    });
  });

  // Test nr. 4
  test('TaskDetails gir riktig lokasjon ved endring', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Venter på at koden har kjørt
    setTimeout(() => {
      // Forventer at inneholder følgende elementer:
      expect(
        wrapper.containsAllMatchingElements([
          <Column>Les leksjon</Column>,
          <Column>Ta notater</Column>,
          // @ts-ignore
          <Form.Checkbox checked={true} />,
        ])
      ).toEqual(true);

      // Simulerer tastetrykk med button-element
      wrapper.find(Button.Success).simulate('click');

      // Setter lokasjon for edit etter koden har kjørt
      setTimeout(() => {
        expect(location.hash).toEqual('#/tasks/1/edit');
        done();
      });
    });
  });

  // Test nr. 5
  test('TaskEdit tegner riktig', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    // Venter på at resten av koden har kjørt
    setTimeout(() => {
      console.log(wrapper.debug());
      // Forventer at inneholder følgende elementer:
      expect(
        wrapper.containsAllMatchingElements([
          // @ts-ignore
          <Form.Input value="Les leksjon" />,
          // @ts-ignore
          <Form.Input value="Ta notater" />,
          // @ts-ignore - ignorerer form.checkbox
          <Form.Checkbox checked={true} />,
        ])
      ).toEqual(false);
      done();
    });
  });

  // Test nr. 6
  test('TaskEdit tegner riktig med snapshots', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();
      done();
    });
  });

  test('TaskEdit gir riktig lokasjon ved lagring', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    // Venter på at resten av koden skal kjøre
    setTimeout(() => {
      // Forventer følgende elementer i wrapper:
      expect(
        wrapper.containsAllMatchingElements([
          // @ts-ignore - ignorerer form.input i testen
          <Form.Input value="Les leksjon" />,
          // @ts-ignore - ignorerer form.input
          <Form.Textarea value="Ta notater" />,
          // @ts-ignore - ignorerer checkbox
          <Form.Checkbox checked={true} />,
        ])
      ).toEqual(true);

      wrapper
        .find(Form.Input)
        .simulate('change', { currentTarget: { value: 'Les leksjon igjen' } });
      // @ts-ignore
      expect(wrapper.containsMatchingElement(<Form.Input value="Les leksjon igjen" />)).toEqual(
        true
      );

      wrapper
        .find(Form.Textarea)
        .simulate('change', { currentTarget: { value: 'Les ekstra nøye' } });
      // @ts-ignore
      expect(wrapper.containsMatchingElement(<Form.Textarea value="Les ekstra nøye" />)).toEqual(
        true
      );

      // Simulerer tastetrykk med button
      wrapper.find(Button.Success).simulate('click');

      setTimeout(() => {
        expect(location.hash).toEqual('#/tasks/1');
        done();
      });
    });
  });

  // Test nr. 6
  test('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Gå tur' } });
    // @ts-ignore
    expect(wrapper.containsMatchingElement(<Form.Input value="Gå tur" />)).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });
});

// Test nr. 8
test('TaskNew gir riktig lokasjon ved sletting', (done) => {
  const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

  setTimeout(() => {
    expect(
      wrapper.containsMatchingElement([
        // @ts-ignore - ignorerer form.input
        <Form.Input value="Les leksjon" />,
        // @ts-ignore - ignorerer form.input
        <Form.Input value="Ta notater" />,
        // @ts-ignore - ignorerer form.checkbox
        <Form.Checkbox checked={true} />,
      ])
    ).toEqual(true);

    // Simulerer tastetrykk på button-element
    wrapper.find(Button.Danger).simulate('click');

    // Venter til koden er ferdig med å kjøre
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/');
      done(); // Indikerer at testen er ferdig
    });
  });
});
