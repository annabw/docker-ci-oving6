// I denne filen setter vi opp slik at vi kan teste React-komponenter

import { configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });
