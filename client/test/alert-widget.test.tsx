import * as React from 'react';
import { Alert } from '../src/widgets';
import { shallow } from 'enzyme';

describe('Alert tests', () => {
  test('No alerts initially', () => {
    const wrapper = shallow(<Alert />);

    expect(wrapper.matchesElement(<div></div>)).toEqual(true);
  });

  test('Show alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <div>
            <div>
              test
              <button />
            </div>
          </div>
        )
      ).toEqual(true);

      done();
    });
  });

  test('Close alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <div>
            <div>
              test
              <button />
            </div>
          </div>
        )
      ).toEqual(true);

      wrapper.find('button.btn-close').simulate('click');

      expect(wrapper.matchesElement(<div></div>)).toEqual(true);

      done();
    });
  });

  // Del 2: Alert test
  test('Open 3 alerts, close the second one', (done) => {
    const wrapper = shallow(<Alert />); // klassen Alert

    //
    Alert.danger('First alert');
    Alert.danger('Second alert');
    Alert.danger('Third alert');

    // Bruker setTimeout for å kjøre koden etter alt annet er kjørt
    // Sjekker at tegningen tilvarer JSX-uttrykket
    setTimeout(() => {
      expect(
        wrapper.containsMatchingElement(
          <div>
            <div>
              First alert <button />
            </div>
            <div>
              Second alert <button />
            </div>
            <div>
              Third alert <button />
            </div>
          </div>
        )
      ).toEqual(true);

      // Simluerer tastetrykk på button-elementer
      wrapper.find('button.btn-close').at(1).simulate('click');

      // Sjekker at tegningen tilsvarer JSX-uttrykket
      expect(
        wrapper.matchesElement(
          <div>
            <div>
              First alert
              <button />
            </div>
            <div>
              Third alert
              <button />
            </div>
          </div>
        )
      ).toEqual(true);

      done();
    });
  });
});
