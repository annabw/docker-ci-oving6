// Her utføres kall mot REST-APIet ved bruk av klassen TaskService
// Importerer axios
import axios from 'axios';

// Setter opp http så vi slipper å gjenta senere
axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Task = {
  id: number;
  title: string;
  done: boolean;
  description: string;
};

// Tjenesteklassen TaskService med metoder
class TaskService {
  // 1: Hent oppgave med gitt id
  get(id: number) {
    return axios.get<Task>('/tasks/' + id).then((response) => response.data);
  }

  // 2: Hent alle oppgaver
  getAll() {
    return axios.get<Task[]>('/tasks').then((response) => response.data);
  }

  // 3: Lag ny oppgave med tittel
  create(title: string, description: string) {
    return axios
      .post<{ id: number }>('/tasks', {
        title: title,
        description: description,
      })
      .then((response) => response.data.id);
  }

  // 4: Oppdater en oppgave, marker som fullført
  // Spesifiserer at vi henter task
  update(task: Task) {
    // oppdaterer task og sender deretter tilbake responsen
    return axios.put<Task[]>('/tasks', task).then((response) => response.data);
  }

  // 5: Slett en oppgave med id
  // Spesifikerer at vi henter id
  delete(id: number) {
    // sletter valgt task og sender deretter tilbake responsen
    return axios.delete<Task[]>('/tasks' + id).then((response) => response.data);
  }
}

// Oppretter taskService og eksporterer den
const taskService = new TaskService();
export default taskService;
