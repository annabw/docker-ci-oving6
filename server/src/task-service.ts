// Service-fil som bruker mysql-pool.ts

import pool from './mysql-pool';

export type Task = {
  id: number;
  title: string;
  description: string; // Oppdatert ihht. databasen
  done: boolean;
};

class TaskService {
  // Hent task med id
  get(id: number) {
    return new Promise<Task | undefined>((resolve, reject) => {
      pool.query('SELECT * FROM Tasks WHERE id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  // Hent alle tasks
  getAll() {
    return new Promise<Task[]>((resolve, reject) => {
      pool.query('SELECT * FROM Tasks', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  // Lag ny oppgave med tittel
  create(title: string, description: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'INSERT INTO Tasks SET title=?, description=?', // Legger til description
        [title, description], // Legger til description
        (error, results) => {
          if (error) return reject(error);

          resolve(Number(results.insertId));
        }
      );
    });
  }

  // Oppdater spesifikk oppgave
  // Setter type
  update(task: Task) {
    // Lager promise
    return new Promise<void>((resolve, reject) => {
      // Utfører databasekall
      pool.query(
        'UPDATE Tasks SET title=?, description=?, done=? WHERE id=?',
        [task.title, task.description, task.done, task.id],
        (error, _results) => {
          if (error) return reject(error); // returnerer error hvis feil

          resolve();
        }
      );
    });
  }

  // Slett task med gitt id
  delete(id: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('DELETE FROM Tasks WHERE id = ?', [id], (error, results) => {
        if (error) return reject(error);

        resolve();
      });
    });
  }
}

const taskService = new TaskService();
export default taskService;
