// Her ligger oppsettet av de ulike REST-kallene

import express from 'express';
import taskService from './task-service';

// express.Router med metoder
const router = express.Router();

// Metoder
// 1: Router for å hente alle tasks
router.get('/tasks', (_request, response) => {
  taskService
    .getAll()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

// 2: Router for å hente en spesifikk task med id
router.get('/tasks/:id', (request, response) => {
  const id = Number(request.params.id);
  taskService
    .get(id)
    .then((task) => (task ? response.send(task) : response.status(404).send('Task not found')))
    .catch((error) => response.status(500).send(error));
});

// 3: Router for å opprette en ny task
router.post('/tasks', (request, response) => {
  const data = request.body;
  if (
    typeof data.title == 'string' &&
    data.title.length != 0 &&
    typeof data.description == 'string'
  )
    taskService
      .create(data.title, data.description)
      .then((id) => response.send({ id: id }))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing task title');
});

// 4: Router for å markere task som fullført
// Bruk PUT for å oppdatere eksisterende oppgave
router.put('/tasks', (request, response) => {
  const data = request.body; // data henter ut det som står i body
  if (
    typeof data.id == 'number' && // dersom id er et tall og
    typeof data.title == 'string' && // tittel er en tekststreng og
    data.title.length != 0 && // tittelens lengde ikke er 0 og
    typeof data.description == 'string' &&
    typeof data.done == 'boolean' // flaggets datatype er ja/nei (boolean)
  )
    taskService // så bruker vi taskService til å..-
      .update({ id: data.id, title: data.title, description: data.description, done: data.done }) // oppdatere oppgavens id, tittel og flagg
      .then(() => response.send()) // og deretter sende responsen
      .catch((error) => response.status(500).send(error));
  // hvis det går galt catcher vi error og sender feilstatus 500
  else response.status(400).send('Oppgaven mangler id, tittel, beskrivelse  eller flagg.'); // hvis ikke sendes feilstatus 400
});

// 5: Router for å slette en spesifikk task
router.delete('/tasks/:id', (request, response) => {
  taskService
    .delete(Number(request.params.id))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

export default router;
