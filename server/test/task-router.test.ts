import axios from 'axios';
import pool from '../src/mysql-pool';
import app from '../src/app';
import taskService, { Task } from '../src/task-service';

const testTasks: Task[] = [
  { id: 1, title: 'Les leksjon', description: 'Ta notater', done: false },
  {
    id: 2,
    title: 'Møt opp på forelesning',
    description: 'Webutvikling. Husk laptop og lader',
    done: false,
  },
  { id: 3, title: 'Gjør øving', description: 'Frist: Fredag 19. november', done: false },
];

// Since API is not compatible with v1, API version is increased to v2
axios.defaults.baseURL = 'http://localhost:3001/api/v2';

let webServer: any;
beforeAll((done) => {
  // Use separate port for testing
  webServer = app.listen(3001, () => done());
});

beforeEach((done) => {
  // Delete all tasks, and reset id auto-increment start value
  pool.query('TRUNCATE TABLE Tasks', (error) => {
    if (error) return done.fail(error);

    // Create testTasks sequentially in order to set correct id, and call done() when finished
    taskService
      .create(testTasks[0].title, testTasks[0].description) // La til description med hjelp fra løsningsforslag
      .then(() => taskService.create(testTasks[1].title, testTasks[1].description)) // Create testTask[1] after testTask[0] has been created
      .then(() => taskService.create(testTasks[2].title, testTasks[2].description)) // Create testTask[2] after testTask[1] has been created
      .then(() => done()); // Call done() after testTask[2] has been created
  });
});

// Stop web server and close connection to MySQL server
afterAll((done) => {
  if (!webServer) return done.fail(new Error());
  webServer.close(() => pool.end(() => done()));
});

// Tester for GET
describe('Fetch tasks (GET)', () => {
  // Tester at vi kan hente alle oppgaver
  test('Fetch all tasks (200 OK)', (done) => {
    axios.get('/tasks').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual(testTasks);
      done();
    });
  });

  // Tester at vi kan hente én oppgave
  test('Fetch task (200 OK)', (done) => {
    axios.get('/tasks/1').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual(testTasks[0]);
      done();
    });
  });

  // Tester feilmelding for å hente oppgave
  test('Fetch task (404 Not Found)', (done) => {
    axios
      .get('/tasks/4')
      .then((_response) => done.fail(new Error()))
      .catch((error) => {
        expect(error.message).toEqual('Request failed with status code 404');
        done();
      });
  });
});

// Tester for PUT
describe('Oppdater task med PUT', () => {
  // Tester at vi kan oppdatere en oppgave
  test('Oppdater oppgave (200 OK)', (done) => {
    axios
      .put('/tasks', {
        id: 1,
        title: 'Pugg leksjonen til eksamen',
        description: 'Ta notater, øv mer',
        done: true,
      })
      .then((response) => {
        // Forventer statuskode 200 OK
        expect(response.status).toEqual(200);
        done(); // Indikerer at testen er ferdig
      });
  });

  // Tester at feilmelding 400 fungerer
  test('Oppdater oppgave (400 Bad Request)', (done) => {
    axios
      .put('/tasks', {
        id: 1,
        title: 'Pugg leksjonen til eksamen',
        description: 'Ta notater, øv mer',
        done: true,
      })
      .catch((error) => {
        // Forventer feilmelding 400 Bad Request
        expect(error.message).toEqual('Feilmelding 400');
        done(); // Indikerer at testen er ferdig
      });
  });
});

// Tester for POST
describe('Create new task (POST)', () => {
  // Tester at vi kan opprette ny oppgave
  test('Create new task (200 OK)', (done) => {
    axios
      .post('/tasks', { title: 'Gå tur', description: 'Frisk luft. Minimum 20 minutter' })
      .then((response) => {
        expect(response.status).toEqual(200);
        expect(response.data).toEqual({ id: 4 });
        done();
      });
  });
});

// Tester feilmelding 400 ved oppretting av ny oppgave
test('Create new task (400)', (done) => {
  axios
    .post('/tasks', { title: 'Gå tur', description: 'Frisk luft. Minimum 20 minutter' })
    .catch((error) => {
      expect(error.message).toEqual('Request failed with status code 400');
      done();
    });
});

// Tester at vi kan slette en oppgave
describe('Delete task (DELETE)', () => {
  test('Delete task (200 OK)', (done) => {
    axios.delete('/tasks/2').then((response) => {
      expect(response.status).toEqual(200);
      done();
    });
  });
});
